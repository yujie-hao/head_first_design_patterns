package test;

import behavior.fly.*;
import models.*;

public class MiniDuckSimulator {
    public static void main(String[] args) {

        //1. ch_1_strategy_pattern.Test mallard duck
        printLine();
        Duck mallard = new MallardDuck();
        mallard.display();
        mallard.performQuack();
        mallard.performFly();
        mallard.swim();

        //2. ch_1_strategy_pattern.Test model duck
        printLine();
        Duck modelDuck = new ModelDuck();
        modelDuck.display();
        modelDuck.performQuack();
        modelDuck.performFly();
        modelDuck.setFlyBehavior(new FlyRocketPowered());
        modelDuck.performFly();
        modelDuck.swim();

        //3. ch_1_strategy_pattern.Test rubber duck
        printLine();
        Duck rubberDuck = new RubberDuck();
        rubberDuck.display();
        rubberDuck.performQuack();
        rubberDuck.performFly();
        rubberDuck.swim();
    }

    public static void printLine() {
        System.out.println("=================");
    }
}
