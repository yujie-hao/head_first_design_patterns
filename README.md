# Head First Design Patterns
To learn a skill, the best way is to practice it.

----------------------------------------------------------------
## Practice procedure:
### 1. Read the book
### 2. Draw UML
#### 2.1 original design
#### 2.2 improved design
### 3. Write code

----------------------------------------------------------------
## UML
### 1. Class Diagram: https://www.smartdraw.com/class-diagram/

----------------------------------------------------------------
## Todo
### 1. Factory pattern
### 2. Singleton pattern
### 3. Adapter and Facade patterns